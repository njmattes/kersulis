Source code for [kersulis.com](kersulis.com). 

Artwork and writing copyright 2018 Nicholas Kersulis, 
except where noted.

Requires `flask`, `flask-images`, `flask-assets`, and
`cssmin`.
 