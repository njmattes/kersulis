#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template


mod = Blueprint('kersulis_main', __name__)


@mod.route('/')
def index():
    return render_template('index.html')


@mod.route('/acquaint')
def acquaint():
    return render_template('acquaint/acquaint.html')


@mod.route('/statement')
def statement():
    return render_template('acquaint/statement.html')


@mod.route('/acknowledgments')
def acknowledgments():
    return render_template('acquaint/acknowledgments.html')


@mod.route('/network')
def network():
    return render_template('acquaint/network.html')


@mod.route('/construction')
def construction():
    return render_template('under_construction.html')


@mod.route('/projects/<_project>')
def project(_project):
    print(_project)
    return render_template(
        'projects/{}.html'.format(_project),
        image_width=1280,
        image_quality=40,
        project=_project
    )


@mod.route('/humans.txt')
def humans():
    return render_template('humans.html')


@mod.route('/copyright.txt')
def copyright():
    return render_template('copyright.txt')
