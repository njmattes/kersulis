from flask import Flask
from flask_assets import Environment, Bundle
from flask_images import Images
from kersulis.views import mod as kersulis_main


app = Flask(__name__)
app.config.from_object('kersulis.config.FlaskConfig')
images = Images(app)

app.register_blueprint(kersulis_main)

assets = Environment(app)
assets.register('img_favicon',
                Bundle('images/favicon.png', output='gen/favicon.png'))
