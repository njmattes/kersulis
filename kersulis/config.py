#!/usr/bin/env python
# -*- coding: utf-8 -*-


class FlaskConfig(object):
    DEBUG = True
    ASSETS_DEBUG = True

    CACHE_KEY_PREFIX = 'kersulis_prod'

    ADMINS = frozenset(['matteson@obstructures.org'])
    SECRET_KEY = 'REPLACEME'

    IMAGES_URL = '/imgsizer'
    IMAGES_PATH = ['static/images', ]
    IMAGES_CACHE = '/tmp/kersulis_resize'

    THREADS_PER_PAGE = 8
