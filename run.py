#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kersulis import app


if __name__ == '__main__':
    app.run()
